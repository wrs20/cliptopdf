from PIL import Image, ImageTk 

import tkinter as tk

import tempfile
import os
import subprocess


def remove_asterix(istr):
    return istr.replace('*', '')

# System commands
pdf_cmd = 'xdg-open'
latex_cmd = 'pdflatex'

# Template for generating a PDF
template = r'''
\documentclass[varwidth]{standalone}
    \usepackage{amsmath,amssymb,amsthm,amsxtra}
    \begin{document}
    \begin{equation*}
        %(CLIPBOARD)s
    \end{equation*}
\end{document}
'''

# Template for generating a PNG
# convert={density=300,size=1080x800,outext=.png}
template2 = r'''
\documentclass[convert, varwidth]{standalone}
    \usepackage{amsmath,amssymb,amsthm,amsxtra}
    \begin{document}
    \begin{equation*}
        %(CLIPBOARD)s
    \end{equation*}
\end{document}
'''

intro_text = r'\text{Copy \LaTeX{} to clipboard}'
error_text = r'\text{\LaTeX{} error}'

class window(tk.Frame):
    def __init__(self, master=None, view=None):
        # Setup Window
        tk.Frame.__init__(self, master)
        self.master.title('LaTeX Clipboard')
        self.canvas = tk.Label(self.master)
        
        # Setup temporary directory
        self.tempdir = tempfile.TemporaryDirectory()
        os.chdir(self.tempdir.name)
        self.filename = os.path.join(self.tempdir.name, 'input.tex')
        self.outname = os.path.join(self.tempdir.name, 'input.png')
        
        # Ensure there is a compilable tex file
        blank = template2 % {'CLIPBOARD': intro_text}
        with open(self.filename, 'w') as fh:
            fh.write(blank)
        
        self.compile_latex()
    
    def copy_clipboard(self):
        # Copy system clipboard to file and write to tex file
        try:
            clip_get = self.clipboard_get()
            tex = template2 % {'CLIPBOARD': clip_get}
        except Exception as e:
            print(e)
            tex = template2 % {'CLIPBOARD': intro_text}
        
        with open(self.filename, 'w') as fh:
            fh.write(tex)
            
        self.compile_latex()
        
    def compile_latex(self):
        # Try and compile latex at command line
        try:
            proc = subprocess.run([latex_cmd, '-interaction=nonstopmode', '--shell-escape', self.filename], check=True, timeout=5)
            print('retcode', proc.returncode)
        except Exception as e:
            print(e)
            self.error_text()
        
        self.draw_latex()
    
    def error_text(self):
        tex = template2 % {'CLIPBOARD': error_text}
        subprocess.run([latex_cmd, '-interaction=nonstopmode', '--shell-escape', self.filename], check=True, timeout=5)
        self.draw_latex()
    
    def draw_latex(self):
        # Open Image
        img = Image.open(self.outname)
        # Convert to compatible (antialiased) mode
        img2 = img.convert('RGBA')
        # Make a TK compatible drawing
        self.tkimage = ImageTk.PhotoImage(img2)
        # Draw to window
        self.canvas.destroy()
        self.canvas = tk.Label(self.master, image=self.tkimage)
        self.canvas.pack()
        # Refresh every so often
        self.master.after(5000, self.copy_clipboard)
    

if __name__ == '__main__':
    gui = window()
    gui.mainloop()
    




