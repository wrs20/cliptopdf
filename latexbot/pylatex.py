
import tempfile
import os
import subprocess



def remove_asterix(istr):
    return istr.replace('*', '')
def remove_newlines(istr):
    return istr.replace('\n', '')


pdf_cmd = 'xdg-open'
latex_cmd = 'pdflatex'

template = r'''
\documentclass[varwidth]{standalone}
    \usepackage{amsmath,amssymb,amsthm,amsxtra}
    \begin{document}
    \begin{equation*}
        %(CLIPBOARD)s
    \end{equation*}
\end{document}
'''

# convert={density=300,size=1080x800,outext=.png}
template2 = r'''
\documentclass[convert, varwidth]{standalone}
    \usepackage{amsmath,amssymb,amsthm,amsxtra}
    \begin{document}
    \begin{equation*}
        %(CLIPBOARD)s
    \end{equation*}
\end{document}
'''

def eqn_to_png(intext):
    return TempPNG(intext)

class TempPNG(object):
    def __init__(self, intext):

        self.tempdir = tempfile.TemporaryDirectory()
        os.chdir(self.tempdir.name)
        filename = os.path.join(self.tempdir.name, 'input.tex')
        outname = None
        
        intext = remove_newlines(intext)

        tex = template2 % {'CLIPBOARD': intext }

        with open(filename, 'w') as fh:
            fh.write(tex)

        print('='*80)
        print(tex)
        print('='*80)

        try:
            subprocess.check_call([latex_cmd,'-interaction', 'scrollmode', '--shell-escape', filename], timeout=4)
            #subprocess.check_call([pdf_cmd, outname])
            outname = os.path.join(self.tempdir.name, 'input.png')

        except Exception as e:
            print(e)
    
        self.outname = outname

if __name__ == '__main__':
    pass





